import { LightningElement,wire } from 'lwc';
import { subscribe,unsubscribe, MessageContext } from 'lightning/messageService';
import BALA from '@salesforce/messageChannel/Bala__c';

export default class Ex25LmsSubscriber extends LightningElement {
    //connected callback //subscribe (messagecontext,Channel name,method name)
    //disconnected callback // unsubscribe ()
    subscription = null;
    @wire(MessageContext)
    messageContext;
    messageValue;
    messageSource;
    accountName;


    // Standard lifecycle hooks used to subscribe and unsubsubscribe to the message channel
    connectedCallback() {
        this.subscribeToMessageChannel();
    }

    subscribeToMessageChannel(){
        this.subscription = subscribe(
            this.messageContext,
            BALA,
            (payLoad) => this.handleMessage(payLoad)
        );
    }

    handleMessage(payLoad) {
        this.receivedMessage = payLoad;
        this.messageValue=payLoad.message;
        this.messageSource=payLoad.source;
        this.accountName=payLoad.recordData.accountName;

      }



}