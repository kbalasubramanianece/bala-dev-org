import { LightningElement } from 'lwc';

export default class Ex14Css extends LightningElement {

    dynamicGreeting = 'World';

    greetingChangeHandler(event){
        this.dynamicGreeting = event.target.value;
}
}