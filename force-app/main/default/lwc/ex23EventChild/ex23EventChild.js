import { LightningElement } from 'lwc';

export default class Ex23EventChild extends LightningElement {

    handleChange(event){
    const number=event.target.value;
    this.dispatchEvent(new CustomEvent('partha',{detail:number}));

    }
}