import { LightningElement } from 'lwc';

export default class Ex22EventChild extends LightningElement {

    handleChange(event){
        const name=event.target.value;
        const nameEvent=new CustomEvent('bala',{detail :name});
        this.dispatchEvent(nameEvent);
    }

}