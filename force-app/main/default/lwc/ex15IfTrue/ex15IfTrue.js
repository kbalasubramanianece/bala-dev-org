import { LightningElement } from 'lwc';

export default class Ex15IfTrue extends LightningElement {
    displayDiv;

    showDivHandler (event){
           this.displayDiv=event.target.checked;
    }

}