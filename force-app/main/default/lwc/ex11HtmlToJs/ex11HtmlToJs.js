import { LightningElement } from 'lwc';

export default class Ex11HtmlToJs extends LightningElement {
    dynamicGreeting = 'World';
    _dynamicGreeting = 'Dummy';
    
    greetingChangeHandler(event){
        this.dynamicGreeting = event.target.value;
}
}