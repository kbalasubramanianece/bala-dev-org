import { LightningElement,api } from 'lwc';

export default class Ex19Child extends LightningElement {

    @api name;
  
    @api displayAlert(){
        alert("Hello! I am an alert box!!");
        this.template.querySelector("c-ex19-grand-child").displayHi();

    }
}